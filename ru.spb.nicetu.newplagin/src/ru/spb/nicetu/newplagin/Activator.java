package ru.spb.nicetu.newplagin;

import org.eclipse.core.databinding.DataBindingContext;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	private static BundleContext context;
	private static DataBindingContext dataBindingContext = null;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}
	
	public static DataBindingContext getDataBindingContextInstance () {
		return dataBindingContext == null ? new DataBindingContext() : dataBindingContext;
	}
	
	

}
