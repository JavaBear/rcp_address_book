package ru.spb.nicetu.newplagin.dataBinding;

import java.util.Date;
import java.util.Objects;

import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.IViewerObservableValue;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.databinding.viewers.ViewerValueProperty;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

public final class BindingConfigurators {
	public BindingConfigurators() {
	}
	
	public static <M> BindingConfigurator<M, Text, String, String> forText(Text text){
		Objects.requireNonNull(text, "Text widget cannot be null");
		
		BindingConfigurator<M, Text, String, String> configurator = new BindingConfigurator<M, Text, String, String>()
				.setWidget(text)
				.setWidgetValueProperty(WidgetProperties.text(SWT.Modify));
		
		return configurator;
	}
	
//	public static <M> BindingConfigurator<M, TableViewer, String, String> forTableViewer(TableViewer viewer) {
//		Objects.requireNonNull(viewer, "Table cannot be null");
//		
//		IViewerObservableValue selectedTodo = ViewerProperties.singleSelection().observe(viewer);
//		IObservableValue<TableViewer> detailValue = PojoProperties.value("colFirstName", String.class).observe(selectedTodo);
//		
//		BindingConfigurator<M, TableViewer, String, String> configurator = new BindingConfigurator<M, TableViewer, String, String>()
//				.setWidget(viewer)
//				.setWidgetValueProperty(selectedTodo);
//	}
	
	public static <M> BindingConfigurator<M, Table, String, String> forTable(Table table) {
		Objects.requireNonNull(table, "Column cannot be null");
		
		
		
		BindingConfigurator<M, Table, String, String> configurator = new BindingConfigurator<M, Table, String, String>()
				.setWidget(table)
				.setWidgetValueProperty(WidgetProperties.singleSelectionIndex());
		
		return configurator;
	}
	
	// ХЗ пока как сделать дату
//	public static <M> BindingConfigurator<M, DateTime, String , DateTime> forDateTime(DateTime date) {
//		Objects.requireNonNull(date, "DateTime widget cannot be null");
//		
//		BindingConfigurator<M, DateTime, java.sql.Date, DateTime> configurator = new BindingConfigurator<M, DateTime, java.sql.Date, DateTime>()
//				.setWidget(date)
//				.setWidgetValueProperty(WidgetProperties.selection());
//		
//		return configurator;
//	}
}
