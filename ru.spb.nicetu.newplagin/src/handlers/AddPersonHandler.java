 
package handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.internal.events.EventBroker;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Shell;

import dialogs.AddPersonDialog;
import ru.spb.nicetu.newplagin.parts.EventConstants;
import ru.spb.nicetu.newplagin.parts.Person;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;

public class AddPersonHandler {
	
	private Person person;
	
	@Inject
	private EventBroker eventBroker;
	
	@Execute
	public void execute(Shell parent) {
		AddPersonDialog dialog = new AddPersonDialog(parent);
		if (dialog.open() == IDialogConstants.OK_ID) {
			person = dialog.getPerson();
			eventBroker.post(EventConstants.ADD_PERSON_TOPIC, person);
		}
	}	
	
	@CanExecute
	public boolean canExecute() {
		
		return true;
	}
		
}