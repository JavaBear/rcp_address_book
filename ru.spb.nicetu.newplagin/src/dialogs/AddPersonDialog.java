package dialogs;

import java.io.File;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.spb.nicetu.newplagin.parts.Person;

public class AddPersonDialog extends Dialog{
	
	private static final String MESSAGE = "Поле должно быть заполнено"; 
	private static final String DECO_MESSAGE = "Нельзя вводить цифры";
	private static final Image DECO_IMAGE = FieldDecorationRegistry.getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_WARNING).getImage();
	
	private Text firstName;
	private Text lastName;
	private Text photo;
	private Person person;
	private Shell shell;
	
	public AddPersonDialog(Shell parent) {
		super(parent);
		shell = parent;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);
		
		firstName = createTextWidget(composite, "Имя:", DECO_MESSAGE);
		lastName = createTextWidget(composite, "Фамилия:", DECO_MESSAGE);
		photo = createTextWidget(composite, "Фотография: ");
		photo.setEditable(false);
		
		Button choosePhoto = new Button(parent, SWT.PUSH);
		choosePhoto.setText("Выбрать фото");
		choosePhoto.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.open();
				photo.setText(dialog.getFilterPath() + "\\" + dialog.getFileName());
			}
		});
		
		return composite;
	}
	
	private Text createTextWidget(Composite parent, String label) {
		return createTextWidget(parent, label, null);
	}
	
	private Text createTextWidget(Composite parent, String label, String decoText) {

		Label label2 = new Label(parent, SWT.NONE);
		label2.setText(label);
		
		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		text.setLayoutData(gridData);
		if (decoText != null) {
			ControlDecoration numDeco = new ControlDecoration(text, SWT.TOP | SWT.LEFT);
			numDeco.setImage(DECO_IMAGE);
			numDeco.setDescriptionText(decoText);
			numDeco.hide();
			
			text.addVerifyListener(new VerifyListener() {
				
				@Override
				public void verifyText(VerifyEvent e) {
					char[] chars = new char[e.text.length()];
					e.text.getChars(0, chars.length, chars, 0);
					for (int i = 0; i < chars.length; i++) {
						if ('0' < chars[i] && chars[i] < '9') {
							e.doit = false;
							numDeco.show();
						} else {
							numDeco.hide();
						}
					}
				}
			});
		}
		
		return text;
		
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Добавить контакт");
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Добавить", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Отменить", false);
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(300, 200);
	}
	
	@Override
	protected void okPressed() {
		if (fieldsAreFilled()) {
			File photoFile = new File(photo.getText());
			if (photoFile.exists() && !photoFile.isDirectory()) {
				person = new Person(firstName.getText(), lastName.getText(), photo.getText());
				super.okPressed();
			} else {
				photo.setText("");
				photo.setMessage("Такого файла не существует");
			}
		}
	}
	
	private boolean fieldsAreFilled() {
		boolean areFilled = true;
		Text[] texts = {firstName, lastName, photo};
		for (Text text : texts) {
			if (text.getText() == null || text.getText().replaceAll(" ", "").equals("")) {
				text.setText("");
				text.setMessage(MESSAGE);
				areFilled = false;
			}
		}
		return areFilled;
	}

	public Person getPerson() {
		if (person != null) return person;
		return null;
	}	
}