package dialogs;

import java.io.File;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ChangePhotoDialog extends Dialog{
	
	private static final String MESSAGE = "Поле должно быть заполнено";
	
	private Text newPhoto;
	private String path;
	private Shell shell;
	
	public ChangePhotoDialog(Shell parent) {
		super(parent);
		shell = parent;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);
		
		newPhoto = createTextWidget(composite, "Новое фото:");
		newPhoto.setEditable(false);
		
		Button choosePhoto = new Button(parent, SWT.PUSH);
		choosePhoto.setText("Выбрать фото");
		choosePhoto.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(shell, SWT.OPEN);
				dialog.open();
				newPhoto.setText(dialog.getFilterPath() + "\\" + dialog.getFileName());
			}
		});
		
		return composite;
		}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Выбрать новое фото");
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Готово", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Отменить", false);
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(300, 125);
	}
	
	@Override
	protected void okPressed() {
		if (fieldIsFilled()) {
			File photo = new File(newPhoto.getText());
			if (photo.exists() && !photo.isDirectory()) {
				path = newPhoto.getText();
				super.okPressed();
			} else {
				newPhoto.setText("");
				newPhoto.setMessage("Такого файла не существует");
			}
		}
	}
	
	public String getPath() {
		return path;
	}
	
	private boolean fieldIsFilled() {
		boolean isFilled = true;
		if (newPhoto.getText() == null || newPhoto.getText().replaceAll(" ", "").equals("")) {
			newPhoto.setText("");
			newPhoto.setMessage(MESSAGE);
			isFilled = false;
		}
		return isFilled;
	}
	
	private Text createTextWidget(Composite parent, String label) {

		Label label2 = new Label(parent, SWT.NONE);
		label2.setText(label);
		
		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		text.setLayoutData(gridData);
		
		return text;
		
	}
	
}
