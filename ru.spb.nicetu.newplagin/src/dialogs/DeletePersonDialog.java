package dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class DeletePersonDialog extends Dialog{
	
	public DeletePersonDialog(Shell parent) {
		super(parent);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite composite = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);
		
		Label label = new Label(composite, SWT.NONE);
		label.setText("Вы уверены, что хотите удалить контакт?");
		
		return composite;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Удалить контакт");
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Удалить", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Отменить", false);
	}
	
//	@Override
//	protected Point getInitialSize() {
//		return new Point(300, 150);
//	}
	
	@Override
	protected void okPressed() {
		super.okPressed();
	}
}
