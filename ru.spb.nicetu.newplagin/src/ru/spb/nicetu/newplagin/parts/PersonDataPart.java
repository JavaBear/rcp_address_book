 
package ru.spb.nicetu.newplagin.parts;

import javax.inject.Inject;

import java.util.Arrays;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.internal.events.EventBroker;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import dialogs.ChangePhotoDialog;
import ru.spb.nicetu.newplagin.Activator;
import ru.spb.nicetu.newplagin.dataBinding.BindingConfigurators;

@Creatable
public class PersonDataPart {
	
	private static final Image DECO_IMAGE = FieldDecorationRegistry.getDefault()
			.getFieldDecoration(FieldDecorationRegistry.DEC_WARNING).getImage();
	
	private Text lastName;
	private Text firstName;
	private Text patronymic;
	private Text city;
	private Text street;
	private Text house;
	private Text building;
	private Text apartment;
	
	private DateTime dateTime;
	
	private Image photo;
	
	Label photoLabel;
	
	private String newPhoto;
	
	private Composite dataComposite;
	private Composite fioAdressComposite;
	private Composite photoComposite;
	private StackLayout stackLayout = new StackLayout();
	private Group fioGroup;	
	private Group photoGroup;
	
	private WritableValue<Person> modelContainer = new WritableValue<>();;
	
	private DataBindingContext dataBindingContext = Activator.getDataBindingContextInstance();
	
	private String[] fieldsNames = new String[] {"lastName", "firstName"
			, "patronymic", "city"
			, "street", "house"
			, "building", "apartment"
	};
	
	private Text[] texts;	
	
	@Inject
	private EventBroker eventBroker;
	
	@Inject
	private Display display;
	
	@Inject
	private EPartService service;

	private Composite mainComposite;
	
	@Inject
	public PersonDataPart() {
		// Do nothing
	}
	
	@PostConstruct
	public void postConstruct(Composite parent) {
		
		// Main composite
		mainComposite = new Composite(parent, SWT.FILL | SWT.CENTER);
		
		// Data composite with stack layout
		dataComposite = new Composite(mainComposite, SWT.FILL | SWT.CENTER);
		dataComposite.setLayout(stackLayout);
		
		fioAdressComposite = new Composite(dataComposite, SWT.FILL | SWT.CENTER);
		
		// FIO group
		fioGroup = new Group(fioAdressComposite, SWT.FILL);
		fioGroup.setText("ФИО");
		
		lastName = createTextWidget(fioGroup, "Фамилия:", true, true);				
		firstName = createTextWidget(fioGroup, "Имя:", true, true);
		patronymic = createTextWidget(fioGroup, "Отчество:", true, true);
		
		dateTime = createDateTimeWidget(fioGroup, "Дата рождения:");
		
		// Address group
		Group address = new Group(fioAdressComposite, SWT.FILL);
		address.setText("Адрес");
			
		city = createTextWidget(address, "Город:", true, true);
		street = createTextWidget(address, "Улица:");
		house = createTextWidget(address, "Дом:", true, false);
		building = createTextWidget(address, "Корпус:");
		apartment = createTextWidget(address, "Квартира:", true, false);
		
		// Photo composite
		photoComposite = new Composite(dataComposite, SWT.FILL | SWT.CENTER);
		// Photo group
		photoGroup = new Group(photoComposite, SWT.FILL);
		photoGroup.setText("Фотография");
		
		// Photo label
		photoLabel = createPhotoLabel(photoGroup);
		photoLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseUp(MouseEvent e) {
				// do nothing
			}
			
			@Override
			public void mouseDown(MouseEvent e) {
				// do nothing
			}
			
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				
				ChangePhotoDialog dialog = new ChangePhotoDialog(parent.getShell());
				if (dialog.open() == IDialogConstants.OK_ID) {
					newPhoto = dialog.getPath();
					photo = new Image(display, newPhoto);
					photoLabel.setImage(photo);
					photoGroup.pack();
				}
			}
		});	
				
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(1).generateLayout(mainComposite);
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(1).generateLayout(fioAdressComposite);
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(4).generateLayout(fioGroup);
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(2).generateLayout(address);
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(1).generateLayout(photoComposite);
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(1).generateLayout(photoGroup);
		
		texts = new Text[] {lastName, firstName
				, patronymic, city
				, street, house
				, building, apartment
		};
		
		bindTextFields();
		
		stackLayout.topControl=fioAdressComposite;		
	}
	
	private void bindTextFields() {
		for (int i = 0; i < texts.length; i++) {
			IValueProperty<Person, String> modelValueProperty = PojoProperties.value(Person.class, fieldsNames[i], String.class);		
			
			BindingConfigurators.<Person>forText(texts[i])
				.setModelValueProperty(modelValueProperty)
				.setModelContainer(modelContainer)
				.setBindingContext(dataBindingContext)
				.configure();
		}
	}
	
	private Label createPhotoLabel(Composite parent) {
		
		Label label = new Label(parent, SWT.FLAT);
		label.setImage(photo);
		
		return label;
	}
	
	private Text createTextWidget(Composite parent, String label) {
		
		return createTextWidget(parent, label, false, false);
		
	}
	
	private Text createTextWidget(Composite parent, String label, boolean needsDeco, boolean numDeco) {

		Label label2 = new Label(parent, SWT.NONE);
		label2.setText(label);
		
		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		text.setEnabled(false);
		
		if (needsDeco) {
			ControlDecoration deco = new ControlDecoration(text, SWT.TOP | SWT.LEFT);
			deco.setImage(DECO_IMAGE);
			deco.hide();
			
			if (numDeco) {
				deco.setDescriptionText("Нельзя вводить цифры");
				text.addVerifyListener(e -> {
					char[] chars = new char[e.text.length()];
					e.text.getChars(0, chars.length, chars, 0);
					for (int i = 0; i < chars.length; i++) {
						if ('0' <= chars[i] && chars[i] <= '9') {
							e.doit = false;
							deco.show();
						} else {
							deco.hide();
						}
					}
				});
			} else {
				deco.setDescriptionText("Нельзя вводить буквы");
				text.addVerifyListener(e -> {
					char[] chars = new char[e.text.length()];
					e.text.getChars(0, chars.length, chars, 0);
					for (int i = 0; i < chars.length; i++) {
						if (!('0' <= chars[i] && chars[i] <= '9')) {
							e.doit = false;
							deco.show();
						} else {
							deco.hide();
						}
					}	
				});
			}
		}
		
		return text;
		
	}
	
	private DateTime createDateTimeWidget(Composite parent, String label) {
		
		Label label2 = new Label(parent, SWT.NONE);
		label2.setText(label);
		
		DateTime dateTime = new DateTime(parent, SWT.DATE | SWT.CALENDAR | SWT.DROP_DOWN);
		dateTime.setEnabled(false);
		
		return dateTime;
	}
	
	// Method for setting enabled text fields, date field and buttons
	private void setWidgetsEnabled (Composite composite) {
		
		Control[] children = composite.getChildren();		
		for (int i = 0; i < children.length; i++) {
			if (children[i].getClass() == Text.class) {
				Text text = (Text) children[i];
				text.setEnabled(!text.getEnabled());
			} else if (children[i].getClass() == DateTime.class) {
				DateTime dateTime = (DateTime) children[i];
				dateTime.setEnabled(!dateTime.getEnabled());
			} else if (children[i].getClass() == Button.class) {
				Button button = (Button) children[i];
				button.setEnabled(!button.getEnabled());
			}
		}
	}
	
	public void changeComposite() {
		if (stackLayout.topControl == photoComposite) {
			stackLayout.topControl = fioAdressComposite;
		} else {
			stackLayout.topControl = photoComposite;
		}
		
		dataComposite.layout(true, true);
		mainComposite.layout(true, true);
	}

	// Method for getting event
	@Inject
	@Optional
	void eventReceived(@UIEventTopic(EventConstants.EDIT_PERSON_TOPIC) Person person) {
		
		if (modelContainer.getValue() == null) {
			Control[] children = fioAdressComposite.getChildren();				
			Stream<Control> stream = Arrays.stream(children);
			stream.forEach(x -> {
				Composite y = (Composite) x;
				setWidgetsEnabled(y);
			});
		}
		
		modelContainer.setValue(person);
	}

	@Inject
	@Optional
	void deletePerson(@UIEventTopic(EventConstants.DELETE_PERSON_TOPIC) Person person) {
		setTextsEmpty();
	}
	
	@Inject
	@Optional
	void changeComposite(@UIEventTopic(EventConstants.CHANGE_COMPOSITE_TOPIC) Object obj) {
		this.changeComposite();
	}
	
	private void setTextsEmpty() {
		lastName.setText("");
		firstName.setText("");
		patronymic.setText("");
		city.setText("");
		street.setText("");
		house.setText("");
		building.setText("");
		apartment.setText("");
	}

}