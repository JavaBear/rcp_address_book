 
package ru.spb.nicetu.newplagin.commands;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;

public class OpenFirstPartCommand {
	
	//TODO 
//	1. Модификаторы доступа к полям класса
//	2. Стринги с ID партов и вьюх должны быть в классах партов и вьюх
//	3. Комментированной код долджен быть удален 
	
	@Inject EPartService partService;
	

	
	@Inject
	private EModelService modelService;
	
	MPart currentPart;
	
//	@Inject
//	@Named ("ru.spb.nicetu.newplagin.menu.newpart")
//	MMenu menu;
	
	private MMenuItem selectedItem;
	String partId = "ru.spb.nicetu.newplagin.handledmenuitem.createdisablefirstpart";
	String newPartId = "ru.spb.nicetu.newplagin.menu.newpart";
	MMenuItem menuItem;
	
	@Execute
	public void execute(final MApplication application) {
		
			if (selectedItem != null && selectedItem.isSelected()) {	
				if	(partService != null) {
					currentPart = partService.showPart("ru.spb.nicetu.newplagin.part.firstpart", PartState.ACTIVATE);
				}
			} else {
				partService.hidePart(currentPart);
			}
	}
	
	
	@CanExecute
	public boolean canExecute(@Optional MMenuItem item) {
		if (item!=null) {
			selectedItem = item;
		}
		return true;
	}
		
}