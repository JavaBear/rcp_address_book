package ru.spb.nicetu.newplagin.dataBinding;

import java.util.Objects;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.swt.widgets.Text;

import ru.spb.nicetu.newplagin.parts.Person;

/* 
 * <M> - My model (Person)
 * <W> - Widget which will be changed
 * <MF> - Model field type
 * <WF> - Widget field type
*/

public class BindingConfigurator<M, W, MF, WF> {
	
	public static class Configuration<M, W> {
		private final WritableValue<M> modelContainer;
		private final W widget;
		private final Binding binding;
		
		public Configuration(Binding binding, WritableValue<M> modelContainer, W widget) {
			this.binding = Objects.requireNonNull(binding);
			this.modelContainer = modelContainer;
			this.widget = Objects.requireNonNull(widget);
		}
		
		public Binding getBinding() {
			return binding;
		}
		
		public WritableValue<M> getModelContainer() {
			return modelContainer;
		}
		
		public W getWidget() {
			return widget;
		}
	}
	

	private DataBindingContext dataBindingContext;
	
	private WritableValue<M> modelContainer;
	
	private M model;
	private W widget;
	
	// For getting IObservableValue
	private IValueProperty<M, MF> modelValueProperty;
	private IValueProperty<W, WF> widgetValueProperty;
	
	// For converting model field data to widget field and back
	private IConverter<MF, WF> modelToWidgetConverter;
	private IConverter<WF, MF> widgetToModelConverter;
	
	protected BindingConfigurator() {
		// Does nothing
	}
	
	public BindingConfigurator<M, W, MF, WF> setBindingContext(DataBindingContext dataBindingContext) {
		this.dataBindingContext = dataBindingContext;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setModelContainer(WritableValue<M> modelContainer) {
		this.modelContainer = modelContainer;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setModel(M model) {
		this.model = model;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setWidget(W widget) {
		this.widget = widget;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setModelValueProperty(IValueProperty<M, MF> modelValueProperty) {
		this.modelValueProperty = modelValueProperty;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setWidgetValueProperty(IValueProperty<W, WF> widgetValueProperty) {
		this.widgetValueProperty = widgetValueProperty;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setModelToWidgetConverter(IConverter<MF, WF> modelToWidgetConverter) {
		this.modelToWidgetConverter = modelToWidgetConverter;
		
		return this;
	}
	
	public BindingConfigurator<M, W, MF, WF> setWidgetToModelConverter(IConverter<WF, MF> widgetToModelConverter) {
		this.widgetToModelConverter = widgetToModelConverter;
		
		return this;
	}
	
	public Configuration<M, W> configure() {
		if (widget == null)
			throw new IllegalStateException("Widget is not set");
		
		DataBindingContext localDataBindingContext = dataBindingContext != null
				? dataBindingContext
				: new DataBindingContext();
		
		WritableValue<M> localModelContainer;
		IObservableValue<MF> modelObservableValue;
		
		if ((modelContainer == null) && (model != null)) {
			localModelContainer = null;
			modelObservableValue = modelValueProperty.observe(model);
		} else {
			localModelContainer = modelContainer != null ? modelContainer : new WritableValue<>();
			
			if (model != null)
				localModelContainer.setValue(model);
			
			modelObservableValue = modelValueProperty.observeDetail(localModelContainer);
		}
		
		IObservableValue<WF> widgetObservableValue = widgetValueProperty.observe(widget);
		
		UpdateValueStrategy<WF, MF> toModelUpdateStrategy = new UpdateValueStrategy<>();
		
		if(widgetToModelConverter != null) {
			toModelUpdateStrategy.setConverter(widgetToModelConverter);
		}
		
		UpdateValueStrategy<MF, WF> toWidgetUpdateStrategy = new UpdateValueStrategy<>();
		
		if(modelToWidgetConverter != null) {
			toWidgetUpdateStrategy.setConverter(modelToWidgetConverter);
		}
		
		Binding localBinding = localDataBindingContext.bindValue(widgetObservableValue, modelObservableValue, toModelUpdateStrategy, toWidgetUpdateStrategy);
		
		return new Configuration<>(localBinding, localModelContainer, widget);
	}
	
	public static <M, MV, V, VV> BindingConfigurator<M, V, MV, VV> newInstance() {
	    return new BindingConfigurator<>();
	}
}
