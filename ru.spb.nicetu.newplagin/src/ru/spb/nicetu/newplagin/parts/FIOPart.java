 
package ru.spb.nicetu.newplagin.parts;

import javax.inject.Inject;

import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import ru.spb.nicetu.newplagin.Activator;


public class FIOPart {
	
	private TableViewer viewer;
	private TableViewerColumn colFirstName;
	private TableViewerColumn colLastName;
	private Person currentPerson;
	private Table table;
	private List<Person> persons;
	private WritableValue<Person> personModel = new WritableValue<>();
	private DataBindingContext dataBindingContext = Activator.getDataBindingContextInstance();
	
	IObservableValue<Person> details;
	
	
	private WritableList<Person> input;
	
	@Inject
	ESelectionService selectionService;
	
	@Inject
	IEclipseContext context;
	
	@Inject
	private IEventBroker eventBroker;
	
	@Inject
	public FIOPart() {
		//Do nothing
	}	
	
	@PostConstruct
	public void postConstruct(Composite parent) {		
		
		
		// Creating new viewer
		viewer = new TableViewer(parent, SWT.H_SCROLL 
				| SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		viewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		// Creating list of persons
		persons = (List<Person>) context.get("list");
		
//		 Creating first name column
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		colFirstName = new TableViewerColumn(viewer, SWT.NONE);
		colFirstName.getColumn().setWidth(75);
		colFirstName.getColumn().setText("Firstname");
		colFirstName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getFirstName();
			}
		});
		
		// Creating last name column
		colLastName = new TableViewerColumn(viewer, SWT.NONE);
		colLastName.getColumn().setWidth(100);
		colLastName.getColumn().setText("Lastname");
		colLastName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getLastName();
			}
		});
		
//		 Filling table
		viewer.setInput(persons);
		
		// Getting table
		table = viewer.getTable();
		
		// Selection listener where we take person data
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Table eventTable = (Table) event.widget;
				// Creating current person for posting his data to another part
				currentPerson = (Person) table.getItem(eventTable.getSelectionIndex()).getData();
				
				// Posting selected person data to another part
				eventBroker.post(EventConstants.EDIT_PERSON_TOPIC, currentPerson);
			}
		});
		
		viewer.addSelectionChangedListener(e -> {
			IStructuredSelection selection = viewer.getStructuredSelection();
			selectionService.setSelection(selection);
		});
		
		GridLayoutFactory.swtDefaults().equalWidth(false).numColumns(1).generateLayout(parent);
		
		// Setting header and lines visible
		table.setHeaderVisible(true);
		table.setLinesVisible(true);	
	}
	
	@Inject
	@Optional
	void addEvent(@UIEventTopic(EventConstants.ADD_PERSON_TOPIC) Person person) {
		persons.add(person);
		
		context.set("list", persons);
		
		viewer.refresh();
	}
	
	@Inject
	@Optional
	void deleteEvent(@UIEventTopic(EventConstants.DELETE_PERSON_TOPIC) Person person) {
		persons.remove(person);
		
		context.set("list", persons);
		
		viewer.refresh();
	}
	
	@Inject
	@Optional
	void setTableEnabled(@UIEventTopic(EventConstants.SET_TABLE_ENABLED_TOPIC) Boolean b) {
		table.setEnabled(b);
		table.setFocus();
	}	
	
	@Focus
	public void setFocus() {
		viewer.getControl().setFocus();
		viewer.refresh();
	}
}

