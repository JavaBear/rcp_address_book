package myapplication;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;

public class OpenNewPartHandler {
	
	@Execute
	public void execute(EPartService partService, MApplication application, EModelService modelService) {
		MPart mPart = modelService.createModelElement(MPart.class);
		mPart.setLabel("Testing");
		mPart.setElementId("newId");
		partService.showPart(mPart, PartState.ACTIVATE);
	}
	
}
