package ru.spb.nicetu.newplagin.parts;

import java.sql.Date;

public class Person implements Comparable<Person> {
	private String firstName;
	private String lastName;
	private String patronymic;
	private Date age;
	private String photo;
	private String city;
	private String street;
	private String house;
	private String building;
	private String apartment;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public Date getAge() {
		return age;
	}

	public void setAge(Date age) {
		this.age = age;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	// Constructor
	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public Person(String firstName, String lastName, String photo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.photo = photo;
	}

	@Override
	public int compareTo(Person o) {
		// If all fields are the same
		if (this.getFirstName().equals(o.getFirstName()) &&
				this.getLastName().equals(o.getLastName()) &&
				this.getPatronymic().equals(o.getPatronymic()) &&
				this.getCity().equals(o.getCity()) &&
				this.getStreet().equals(o.getStreet()) &&
				this.getHouse().equals(o.getHouse()) &&
				this.getBuilding().equals(o.getBuilding()) &&
				this.getApartment().equals(o.getApartment())) return 0;
		else return 1;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", patronymic=" + patronymic + ", age="
				+ age + ", photo=" + photo + ", city=" + city + ", street=" + street + ", house=" + house
				+ ", building=" + building + ", apartment=" + apartment + "]";
	}
	
	
}
