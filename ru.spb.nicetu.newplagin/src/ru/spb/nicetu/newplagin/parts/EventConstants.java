package ru.spb.nicetu.newplagin.parts;

public class EventConstants {
	public static final String SAVE_TOPIC = "SAVE_TOPIC";
	public static final String EDIT_PERSON_TOPIC = "EDIT_PERSON_TOPIC";
	public static final String ADD_PERSON_TOPIC = "ADD_PERSON_TOPIC";
	public static final String DELETE_PERSON_TOPIC = "DELETE_PERSON_TOPIC";
	public static final String CHANGE_COMPOSITE_TOPIC = "CHANGE_COMPOSITE_TOPIC";
	public static final String CHANGE_PHOTO_TOPIC = "CHANGE_PHOTO_TOPIC"; 
	public static final String SET_TABLE_ENABLED_TOPIC = "SET_TABLE_ENABLED_TOPIC";
}
