package lifecycle;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.contexts.RunAndTrack;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;

import ru.spb.nicetu.newplagin.parts.Person;

public class LifeCycleManager {
	
	private final static String PATH = "D:\\\\Test\\\\data.txt";
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	private List<Person> persons;

	@PostContextCreate
	public void postContextCreate(IEclipseContext context) {
		persons = readFile();
		context.set("list", persons);
	}
	
	@PreSave
	public void preSave(IEclipseContext context) {
		persons = (List<Person>) context.get("list");
		context.runAndTrack(new RunAndTrack() {
			
			@Override
			public boolean changed(IEclipseContext context) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		writeFile(persons);
	}
	
	// Reading file
	private List<Person> readFile() {
		List<Person> lPersons = new ArrayList<>();
		Person person;
		
		try (FileInputStream fStream = new FileInputStream(PATH)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
			String string;
			
			while ((string = br.readLine()) != null) {
				String[] data = string.split(",");
				person = setPersonData(data);
				lPersons.add(person);
			}
		} catch (IOException exception) {
			exception.getMessage();
		}
		
		return lPersons;
	}
	
	private Person setPersonData(String[] data) {
		Person person = null;
		try {
			person = new Person(data[0], data[1]);
			person.setPatronymic(data[2]);
			person.setAge(getDate(data[3]));
			person.setPhoto(data[4]);
			person.setCity(data[5]);
			person.setStreet(data[6]);
			person.setHouse(data[7]);
			person.setBuilding(data[8]);
			person.setApartment(data[9]);
		} catch (ArrayIndexOutOfBoundsException exception) {
			
		}
		return person;
	}
	
	private java.sql.Date getDate(String data) {
		if (!data.isEmpty()) {
			String[] dateDate = data.split("/");
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateDate[0]));
			cal.set(Calendar.MONTH, Integer.parseInt(dateDate[1]) - 1);
			cal.set(Calendar.YEAR, Integer.parseInt(dateDate[2]));
			long timeInMillis = cal.getTimeInMillis();
			return new java.sql.Date(timeInMillis);
		} else {
			Date d = new Date();
			Calendar cal = Calendar.getInstance();
			long timeInMillis = d.getTime();
			cal.setTimeInMillis(timeInMillis);
			return new java.sql.Date(timeInMillis);
		}
	}
	
	// Writing file
	private void writeFile(List<Person> lPerson) {
		// Clean file and write new data
		cleanFile();
		for (Person person : lPerson) {
			String data = getPersonData(person);
			try (FileWriter writer = new FileWriter(PATH, true)) {
				writer.write(data);
			} catch (IOException exception) {
				exception.getMessage();
			}
		}
	}
	
	private void cleanFile() {
		try (FileWriter writer = new FileWriter(PATH, false)) {
			writer.write("");
		} catch (IOException exception) {
			exception.getMessage();
		}
	}
	
	private String getPersonData (Person person) {
		
		if (person.getAge() == null) {
			return "" + person.getFirstName() + ","
					+ person.getLastName() + ","
					+ (person.getPatronymic() != null ? person.getPatronymic(): "") + "," 
					+ "" + ","
					+ (person.getPhoto() != null ? person.getPhoto(): "") + ","
					+ (person.getCity() != null ? person.getCity(): "") + ","
					+ (person.getStreet() != null ? person.getStreet(): "") + ","
					+ (person.getHouse() != null ? person.getHouse(): "") + ","
					+ (person.getBuilding() != null ? person.getBuilding(): "") + ","
					+ (person.getApartment() != null ? person.getApartment(): "") + "\n";
		}
		return "" + person.getFirstName() + ","
					+ person.getLastName() + ","
					+ (person.getPatronymic() != null ? person.getPatronymic(): "") + "," 
					+ DATE_FORMAT.format(person.getAge()) + ","
					+ (person.getPhoto() != null ? person.getPhoto(): "") + ","
					+ (person.getCity() != null ? person.getCity(): "") + ","
					+ (person.getStreet() != null ? person.getStreet(): "") + ","
					+ (person.getHouse() != null ? person.getHouse(): "") + ","
					+ (person.getBuilding() != null ? person.getBuilding(): "") + ","
					+ (person.getApartment() != null ? person.getApartment(): "") + "\n";
	}
}
