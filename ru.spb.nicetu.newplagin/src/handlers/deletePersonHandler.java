package handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.internal.events.EventBroker;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;

import dialogs.DeletePersonDialog;
import ru.spb.nicetu.newplagin.parts.EventConstants;
import ru.spb.nicetu.newplagin.parts.Person;

public class deletePersonHandler {

	private Person person;
	
	@Inject
	private ESelectionService selectionService;
	
	@Inject
	private EventBroker eventBroker;
	
	private boolean enabled = true;
	
	@Execute
	public void execute(Shell parent) {
		IStructuredSelection selection = (IStructuredSelection) selectionService.getSelection();
		
		person = (Person) selection.getFirstElement();
		
		DeletePersonDialog dialog = new DeletePersonDialog(parent);
		if (dialog.open() == IDialogConstants.OK_ID) {
			eventBroker.post(EventConstants.DELETE_PERSON_TOPIC, person);
		}
	}
	
	
	@CanExecute
	public boolean canExecute() {		
		
		IStructuredSelection selection = (IStructuredSelection) selectionService.getSelection();
		
		return (selection!= null && selection.getFirstElement() instanceof Person) && enabled;
	}	
	
	@Inject
	@Optional
	public void enabled(@UIEventTopic(EventConstants.SET_TABLE_ENABLED_TOPIC) Boolean b) {
		enabled = b;
	}
}