 
package handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.internal.events.EventBroker;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.IStructuredSelection;

import ru.spb.nicetu.newplagin.parts.EventConstants;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;

public class DataAndPhotoHandler {
	
	@Inject
	private EventBroker event;
	
	@Execute
	public void execute() {
		event.post(EventConstants.CHANGE_COMPOSITE_TOPIC, null);
	}
	
	
	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {
		
		IStructuredSelection selection = (IStructuredSelection) selectionService.getSelection("ru.spb.nicetu.newplagin.part.0");
		
		if (selection!= null) {
			return true;
		}
		return false;
	}
		
}